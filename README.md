# README #
Prototype of LynxApp, Android app to show the ingredients of a product using NFC technology to read the label. 
Project for Mobile Media Design course (IMT2661) at NTNU i Gjoevik.


###Programming Language ###
Android (Java, XML).

### Versions supported ###
Android 4.0.3 and +.

### Owners###

* Jaime Moreno Quintanar (jaimemq@stud.ntnu.no)
* Andrés Escalante Ariza
* David Eguizabal Pérez