package com.taco.lynxapp;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

public class IngredientsSelection extends AppCompatActivity {

    CheckBox c1;
    CheckBox c2;
    CheckBox c3;
    CheckBox c4;
    CheckBox c5;
    Button btnSave;
    Button btnDelete;
    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients_selection);

        c1 = (CheckBox) findViewById(R.id.checkBox1);
        c2 = (CheckBox) findViewById(R.id.checkBox2);
        c3 = (CheckBox) findViewById(R.id.checkBox3);
        c4 = (CheckBox) findViewById(R.id.checkBox4);
        c5 = (CheckBox) findViewById(R.id.checkBox5);
        btnSave = (Button) findViewById(R.id.button3);
        btnDelete = (Button) findViewById(R.id.button5);

        myDb = new DatabaseHelper(this);

        showAll();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myDb.deleteAll();
                if (c1.isChecked()){
                    long  comparition = compare(c1.getText().toString());
                    if (comparition == 0){
                        boolean result = myDb.insertData(c1.getText().toString());
                        /*if (result) {
                            Toast.makeText(getApplicationContext(), "TRUE", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "FALSE", Toast.LENGTH_LONG).show();

                        }*/
                    }

                }
                if (c2.isChecked()){
                    long  comparition = compare(c2.getText().toString());
                    if (comparition == 0){
                        boolean result = myDb.insertData(c2.getText().toString());
                        /*if (result) {
                            Toast.makeText(getApplicationContext(), "TRUE", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "FALSE", Toast.LENGTH_LONG).show();

                        }*/
                    }
                }
                if (c3.isChecked()){
                    long  comparition = compare(c3.getText().toString());
                    if (comparition == 0){
                        boolean result = myDb.insertData(c3.getText().toString());
                        /*if (result) {
                            Toast.makeText(getApplicationContext(), "TRUE", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "FALSE", Toast.LENGTH_LONG).show();

                        }*/
                    }
                }
                if (c4.isChecked()){
                    long  comparition = compare(c4.getText().toString());
                    if (comparition == 0){
                        boolean result = myDb.insertData(c4.getText().toString());
                        /*if (result) {
                            Toast.makeText(getApplicationContext(), "TRUE", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "FALSE", Toast.LENGTH_LONG).show();

                        }*/
                    }
                }
                if (c5.isChecked()){
                    long  comparition = compare(c5.getText().toString());
                    if (comparition == 0){
                        boolean result = myDb.insertData(c5.getText().toString());
                        /*if (result) {
                            Toast.makeText(getApplicationContext(), "TRUE", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(getApplicationContext(), "FALSE", Toast.LENGTH_LONG).show();

                        }*/
                    }
                }
                viewAll();

            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (c1.isChecked()){
                    c1.toggle();
                }
                if (c2.isChecked()){
                    c2.toggle();
                }
                if (c3.isChecked()){
                    c3.toggle();
                }
                if (c4.isChecked()){
                    c4.toggle();
                }
                if (c5.isChecked()){
                    c5.toggle();
                }

                myDb.deleteAll();
                viewAll();

            }
        });

    }

    public void viewAll(){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            //Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            buffer.append("Id :"+ res.getString(0)+"\n");
            buffer.append("Year:"+ res.getString(1)+"\n");

        }

        // Show all data
        //Toast.makeText(this, buffer.toString(), Toast.LENGTH_LONG).show();
    }

    //Method to show all the items in the database
    public void showAll(){
        Cursor res = myDb.getAllData();
        if (res.getCount() != 0){
            while (res.moveToNext()){

                if (res.getString(1).equals(c1.getText().toString())){
                    c1.toggle();
                }else if (res.getString(1).equals(c2.getText().toString())){
                    c2.toggle();
                }else if (res.getString(1).equals(c3.getText().toString())){
                    c3.toggle();
                }else if (res.getString(1).equals(c4.getText().toString())){
                    c4.toggle();
                }else {
                    c5.toggle();
                }
            }
        }

    }

    public long compare(String string){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            //Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
            return 0;
        }

        while (res.moveToNext()) {
            if (string.equals(res.getString(1))){
                return 1;
            }

        }
        return 0;
    }
}