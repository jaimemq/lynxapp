package com.taco.lynxapp;

import android.database.Cursor;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

/**
 * Code from: https://www.youtube.com/watch?v=yeyUWcn9Zms
 */
public class Display extends AppCompatActivity implements TextToSpeech.OnInitListener{

    private TextToSpeech textToSpeech;
    private FloatingActionButton floatingActionButton;
    private TextView textView, textView2,textView3,textView4,textView5,textView6,textView7,textView8,textView9,textView10;

    DatabaseHelper myDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        textToSpeech = new TextToSpeech(this, this);
        floatingActionButton = (FloatingActionButton)findViewById(R.id.floatingActionButton);
        textView = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);
        textView5 = (TextView) findViewById(R.id.textView5);
        textView6 = (TextView) findViewById(R.id.textView6);
        textView7 = (TextView) findViewById(R.id.textView7);
        textView8 = (TextView) findViewById(R.id.textView8);
        textView9 = (TextView) findViewById(R.id.textView9);
        textView10 = (TextView) findViewById(R.id.textView10);

        myDb = new DatabaseHelper(this);
        //viewAll();
        compareIngredients();





        //if(textView.getText().toString().equals(getResources().getString(R.string.txt_long))){
            //textView.setBackgroundColor(getResources().getColor(R.color.colorAccent));
        //}
        
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                speakOut();
            }
        });
    }

    private void speakOut() {
        String text = textView.getText().toString();
        String text2 = textView2.getText().toString();
        String text3 = textView3.getText().toString();
        String text4 = textView4.getText().toString();
        String text5 = textView5.getText().toString();
        String text6 = textView6.getText().toString();
        String text7 = textView7.getText().toString();
        String text8 = textView8.getText().toString();
        String text9 = textView9.getText().toString();
        String text10 = textView10.getText().toString();

        String textLong = text+", "+text2+", "+text3+", "+text4+", "+text5+", "+text6+", "+text7+", "+text8+", "+text9+", "+text10;
        textToSpeech.setSpeechRate((float)0.5);
        textToSpeech.speak(textLong, TextToSpeech.QUEUE_FLUSH, null);
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS){
            int result = textToSpeech.setLanguage(Locale.ENGLISH);
            if (result == TextToSpeech.LANG_NOT_SUPPORTED || result == TextToSpeech.LANG_MISSING_DATA){
                Log.e("TTS", "This language is not supported");
            }else{
                floatingActionButton.setEnabled(true);
                speakOut();
            }
        }else{
            Log.e("TTS", "Initialization failed");
        }
    }

    public void compareIngredients(){
        long compare = compare(textView2.getText().toString());
        if (compare == 1){
            textView2.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView3.getText().toString());
        if (compare == 1){
            textView3.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView4.getText().toString());
        if (compare == 1){
            textView4.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView5.getText().toString());
        if (compare == 1){
            textView5.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView6.getText().toString());
        if (compare == 1){
            textView6.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView7.getText().toString());
        if (compare == 1){
            textView7.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView8.getText().toString());
        if (compare == 1){
            textView8.setBackgroundColor(getResources().getColor(R.color.red));
        }
        compare = compare(textView9.getText().toString());
        if (compare == 1){
            textView9.setBackgroundColor(getResources().getColor(R.color.red));
        }

    }
    public long compare(String string){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            //Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
            return 0;
        }

        while (res.moveToNext()) {
            if (string.equals(res.getString(1))){
                return 1;
            }

        }
        return 0;
    }


    public void viewAll(){
        Cursor res = myDb.getAllData();
        if(res.getCount() == 0) {
            // show message
            //Toast.makeText(this, "Empty", Toast.LENGTH_SHORT).show();
            return;
        }

        StringBuffer buffer = new StringBuffer();
        while (res.moveToNext()) {
            buffer.append("Id :"+ res.getString(0)+"\n");
            buffer.append("Year:"+ res.getString(1)+"\n");

        }

        // Show all data
        Toast.makeText(this, buffer.toString(), Toast.LENGTH_LONG).show();
    }
}
