package com.taco.lynxapp;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ImageButton button;

    NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        // check is the device have NFC and that is activated
        if (nfcAdapter!=null && nfcAdapter.isEnabled()){
            Toast.makeText(this, "NFC avalible", Toast.LENGTH_SHORT).show();
        } else{
            Toast.makeText(this, "NFC not avalible", Toast.LENGTH_SHORT).show();
            //finish();
        }


        /**
         * code for blinking button taken from: http://stackoverflow.com/questions/4852281/android-how-can-i-make-a-button-flash
         */
        final Animation animation = new AlphaAnimation(1, (long)0.9); // Change alpha from fully visible to invisible
        animation.setDuration(1000); // duration - half a second
        animation.setInterpolator(new LinearInterpolator()); // do not alter animation rate
        animation.setRepeatCount(Animation.INFINITE); // Repeat animation infinitely
        animation.setRepeatMode(Animation.REVERSE); // Reverse animation at the end so the button will fade back in




        button = (ImageButton) findViewById(R.id.button);

        button.startAnimation(animation);

        button.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.button:
                Intent intent = new Intent(this, Display.class);
                startActivity(intent);
                break;
            default:
                break;


        }
    }

    // Menu icons are inflated just as they were with actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.set_icon:
                Intent intent2 = new Intent(this, IngredientsSelection.class);
                startActivity(intent2);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        //When an intent is received it looks is it a NFC

        super.onNewIntent(intent);
        if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
            Toast.makeText(this, "NfcIntent!", Toast.LENGTH_SHORT).show();
            if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
                //Create a tag with the data received with the intent
                Tag myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

                //send the Tag Id using the function send()
                Toast.makeText(this, bytesToHexString(myTag.getId()),Toast.LENGTH_SHORT).show();
                Intent intent2 = new Intent(this, Display.class);
                startActivity(intent2);
            }
        }
    }


    @Override
    protected void onResume() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,0,intent,0);

        IntentFilter[] intentFilter = new IntentFilter[2];
        intentFilter[0]= new IntentFilter();
        intentFilter[0].addAction(NfcAdapter.ACTION_TECH_DISCOVERED);
        intentFilter[0].addCategory(Intent.CATEGORY_DEFAULT);
        intentFilter[1]= new IntentFilter();
        intentFilter[1].addAction(NfcAdapter.ACTION_TAG_DISCOVERED);
        intentFilter[1].addCategory(Intent.CATEGORY_DEFAULT);

        if(nfcAdapter!=null){

            nfcAdapter.enableForegroundDispatch(this, pendingIntent,intentFilter,null);
        }
        super.onResume();

    }



    /* transform bytes array into hexadecimal string
    code from: http://stackoverflow.com/questions/6060312/how-do-you-read-the-unique-id-of-an-nfc-tag-on-android
    */
    private String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("0x");
        if (src == null || src.length <= 0) {
            return null;
        }

        char[] buffer = new char[2];
        for (int i = 0; i < src.length; i++) {
            buffer[0] = Character.forDigit((src[i] >>> 4) & 0x0F, 16);
            buffer[1] = Character.forDigit(src[i] & 0x0F, 16);
            System.out.println(buffer);
            stringBuilder.append(buffer);
        }

        return stringBuilder.toString();
    }
}
